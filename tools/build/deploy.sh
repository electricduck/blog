mkdir ~/.ssh                                                                # install SSH key
echo $BB_SSHKEY > ~/.ssh/id_rsa.tmp                                         # see: https://answers.atlassian.com/questions/39243415/how-can-i-use-ssh-in-bitbucket-pipelines
base64 -d ~/.ssh/id_rsa.tmp > ~/.ssh/id_rsa                                 #
chmod 600 ~/.ssh/id_rsa                                                     #
base64 ~/.ssh/id_rsa                                                        #
ssh -o StrictHostKeyChecking=no $BB_SSHUSER@$BB_SSHSERVER "${BB_BUILDCMD}"  # build on remote server